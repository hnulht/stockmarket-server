# stockmarket-server

### 项目介绍
HNU夏季小学期移动应用开发课程设计《股票市场分析系统》服务器端🤗

### 软件架构

#### 开发环境
- 语言：Java
- JDK：1.8
- IDE：IDEA
- 数据库：MySQL
- 依赖管理：Maven

#### 技术框架
- 核心框架：Spring Boot
- 持久层框架：MyBatis-Plus
- 日志工具：Slf4j
- 文档生产工具：Swagger
- 简化对象封装工具：Lombok
- 安全框架：JWT
- Java工具集：Hutool
- 验证框架：Hibernator-Validator
- 序列化和反序列化工具：Fastjson

#### 项目特征
- 后台接口Restful风格，支持前后端分离
- Token角色权限认证
- 统一响应结果封装及生成工具
- 统一异常处理
- 提供代码生成器
- 自动生成接口文档

#### 代码地址
- 服务器端：https://gitee.com/hnulht/stockmarket-server
- 微服务器端&用户Web端：https://gitee.com/hnulht/stockmarket-microserver
- 用户Android端：https://gitee.com/hnulht/stockmarket-app
- 管理员Web端：https://gitee.com/WilfredShen/stockmarket-vue
- 用户Web端和Android端内嵌web界面：https://gitee.com/WilfredShen/stockmarket-app

#### 技术架构图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/133758_a9c82222_7864978.jpeg "stockmarket_framework.JPG")

### 数据库设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/193933_ba3bd2c5_7864978.png "stockmarket_sql.png")

### 使用说明
- 使用IDEA（安装Lombok插件）导入项目
- 创建数据库stockmarket并导入stockmarket.sql
- 配置application-dev.yml中的数据库连接信息
- 直接运行StockmarketApplication
- 命令行执行`java -jar stockmarket-0.0.1-SNAPSHOT.jar`

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/193836_39cf9813_7864978.png "stockmarket_jar.png")

- 接口文档访问http://127.0.0.1:8081/swagger-ui/

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/193845_e936cfa1_7864978.png "stockmarket_swagger.png")