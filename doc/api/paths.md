
<a name="paths"></a>
## 资源

<a name="7df6e04761d274189ae2f1726f40807d"></a>
### 公共数据接口
Base Controller


<a name="swaggermodelusingpost_1"></a>
#### Swagger添加模型
```
POST /swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="2a5bc73dd80008d19fd9d68b1d94f01c"></a>
### 历史A股K线数据接口
History K Data Plus Controller


<a name="updatehistorykdataplususingpost"></a>
#### 获取指定的历史A股K线数据
```
POST /history_k_data_plus/query_history_k_data_plus
```


##### 说明
获取指定的历史A股K线数据信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_2"></a>
#### Swagger添加模型
```
POST /history_k_data_plus/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="dff318b2d21d1da51ac32a2ab9ef1e71"></a>
### 涨跌幅数据接口
Stock Chg Controller


<a name="listfallstockchgusingpost"></a>
#### 分页查询跌幅榜
```
POST /stock_chg/list_fall
```


##### 说明
分页查询所有跌幅榜信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="listquerystockchgusingpost"></a>
#### 根据用户输入的信息查询涨跌幅
```
POST /stock_chg/list_query
```


##### 说明
根据用户输入的信息查询涨跌幅信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="listrisestockchgusingpost"></a>
#### 分页查询涨幅榜
```
POST /stock_chg/list_rise
```


##### 说明
分页查询所有涨幅榜信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_5"></a>
#### Swagger添加模型
```
POST /stock_chg/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updatestockchgusingget"></a>
#### 更新涨跌幅
```
GET /stock_chg/update_stock_chg
```


##### 说明
更新涨跌幅信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="ae90bc5914049836967cb6141d10c266"></a>
### 用户数据接口
User Controller


<a name="getuserbytokenusingget"></a>
#### 获取用户
```
GET /user/get
```


##### 说明
根据Token获取用户信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|获取成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**400**|获取失败|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="loginusingpost_1"></a>
#### 用户登录
```
POST /user/login
```


##### 说明
根据用户输入的信息检验登录


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|登录成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|密码不正确|无内容|
|**404**|用户未注册|无内容|
|**500**|服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="logoutusingpost"></a>
#### 用户退出登录
```
POST /user/logout
```


##### 说明
用户/管理员退出登录


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|退出登录成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|退出登录失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="registerusingpost"></a>
#### 用户注册
```
POST /user/register
```


##### 说明
根据用户输入的信息注册用户


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|注册成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|用户名已存在|无内容|
|**404**|Not Found|无内容|
|**500**|服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_8"></a>
#### Swagger添加模型
```
POST /user/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updateuserbyidusingput_1"></a>
#### 更新用户
```
PUT /user/update
```


##### 说明
根据id更新用户信息, 传入待更新的用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**user**  <br>*必填*|user|[用户实体类](#c2d667b8a3f2d3260c2fffc9b5454834)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updateuserpasswordusingput"></a>
#### 更新用户密码
```
PUT /user/updateUserPassword
```


##### 说明
根据id更新更新用户密码, 传入待更新的用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**user**  <br>*必填*|user|[用户实体类](#c2d667b8a3f2d3260c2fffc9b5454834)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="556c0103e55ffd07eef9ba8ef37fda51"></a>
### 管理员数据接口
Admin Controller


<a name="countallusersusingget"></a>
#### 查询总用户数
```
GET /admin/count
```


##### 说明
查询总用户数


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="getuserbyidusingpost"></a>
#### 查询用户
```
POST /admin/get/{id}
```


##### 说明
根据id查询用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="listallusersusingpost"></a>
#### 分页查询所有用户
```
POST /admin/list
```


##### 说明
分页查询所有用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="loginusingpost"></a>
#### 管理员登录
```
POST /admin/login
```


##### 说明
根据管理员输入的信息检验登录


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|登录成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|权限不足|无内容|
|**403**|密码不正确|无内容|
|**404**|用户未注册|无内容|
|**500**|服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="removeuserbyidusingdelete"></a>
#### 删除用户
```
DELETE /admin/remove/{id}
```


##### 说明
根据id删除用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|删除成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**500**|删除失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="saveuserusingpost"></a>
#### 插入用户
```
POST /admin/save
```


##### 说明
插入用户信息, 传入待插入的用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**user**  <br>*必填*|user|[用户实体类](#c2d667b8a3f2d3260c2fffc9b5454834)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|插入成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|插入失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost"></a>
#### Swagger添加模型
```
POST /admin/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updateuserbyidusingput"></a>
#### 更新用户
```
PUT /admin/update
```


##### 说明
根据id更新用户信息, 传入待更新的用户信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**user**  <br>*必填*|user|[用户实体类](#c2d667b8a3f2d3260c2fffc9b5454834)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="verifypermissionusingpost"></a>
#### 验证权限
```
POST /admin/verify
```


##### 说明
验证管理员或用户浏览页面的权限


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|验证成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="afe3a2690554ef238dfa379b779ffa5c"></a>
### 股票指数数据接口
Stock Index Controller


<a name="liststockindexusingget"></a>
#### 查询所有股票指数数据
```
GET /stock_index/list_stock_index
```


##### 说明
查询所有股票指数数据信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="swaggermodelusingpost_6"></a>
#### Swagger添加模型
```
POST /stock_index/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updatestockindexusingget"></a>
#### 更新股票指数数据
```
GET /stock_index/update_stock_index
```


##### 说明
更新股票指数数据信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="ce43408db623f7c398e830e3ba4a3429"></a>
### 行业分类数据接口
Stock Industry Controller


<a name="listallstockindustryusingpost"></a>
#### 分页查询所有行业分类
```
POST /stock_industry/list
```


##### 说明
分页查询所有行业分类信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_7"></a>
#### Swagger添加模型
```
POST /stock_industry/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updatestockindustryusingget"></a>
#### 更新行业分类
```
GET /stock_industry/update_stock_industry
```


##### 说明
更新所有行业分类信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="b0823a815ca7be7bc497d97e5e07a534"></a>
### 证券基本资料数据接口
Stock Basic Controller


<a name="listallstockbasicusingpost"></a>
#### 分页查询所有证券基本资料
```
POST /stock_basic/list
```


##### 说明
分页查询所有证券基本资料信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_3"></a>
#### Swagger添加模型
```
POST /stock_basic/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="updatestockbasicusingget"></a>
#### 更新证券基本资料
```
GET /stock_basic/update_stock_basic
```


##### 说明
更新所有证券基本资料信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|更新成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|更新失败/服务器内部错误|无内容|


##### 生成

* `*/*`


<a name="0849db691643cb43a399ab25a6a06db0"></a>
### 证券基本资料行业分类数据接口
Stock Basic Industry Controller


<a name="listallstockbasicindustryusingpost"></a>
#### 分页查询所有证券基本资料行业分类
```
POST /stock_basic_industry/list
```


##### 说明
分页查询所有证券基本资料行业分类信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="listquerystockbasicindustryusingpost"></a>
#### 根据用户输入的信息查询证券基本资料行业分类
```
POST /stock_basic_industry/list_query
```


##### 说明
根据用户输入的信息查询证券基本资料行业分类信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**jsonObject**  <br>*必填*|jsonObject|< string, object > map|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|查询成功|[响应信息类](#54b45f79d3fdf41d46ba93346df5e481)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|
|**500**|查询失败/服务器内部错误|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


<a name="swaggermodelusingpost_4"></a>
#### Swagger添加模型
```
POST /stock_basic_industry/swagger
```


##### 说明
Swagger添加模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**baseEntity**  <br>*必填*|baseEntity|[公共父类实体类](#91299b03037c15057d9679d73ef42857)|
|**Body**|**stockBasic**  <br>*必填*|stockBasic|[证券基本资料实体类](#4998e7e76311eb24f89bc35726c32174)|
|**Body**|**stockBasicIndustry**  <br>*必填*|stockBasicIndustry|[证券基本资料行业分类实体类](#a78b933b7c5f1a13461c04f4ef66ffaa)|
|**Body**|**stockChg**  <br>*必填*|stockChg|[涨跌幅实体类](#00c7c47c76a9ae0888fcb30a58a91cb4)|
|**Body**|**stockIndustry**  <br>*必填*|stockIndustry|[行业分类实体类](#e8b9b7c8da3592b2e6c6cb2dd180366b)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`



