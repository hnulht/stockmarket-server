# 股票市场分析系统API测试文档


<a name="overview"></a>
## 概览
接口测试文档


### 版本信息
*版本* : v1.0


### 联系方式
*名字* : hnulht  
*邮箱* : 381874042@qq.com


### 许可信息
*许可证* : Apache2.0  
*许可网址* : http://www.apache.org/licenses/LICENSE-2.0  
*服务条款* : null


### URI scheme
*域名* : localhost:8081  
*基础路径* : /


### 标签

* 公共数据接口 : Base Controller
* 历史A股K线数据接口 : History K Data Plus Controller
* 涨跌幅数据接口 : Stock Chg Controller
* 用户数据接口 : User Controller
* 管理员数据接口 : Admin Controller
* 股票指数数据接口 : Stock Index Controller
* 行业分类数据接口 : Stock Industry Controller
* 证券基本资料数据接口 : Stock Basic Controller
* 证券基本资料行业分类数据接口 : Stock Basic Industry Controller



