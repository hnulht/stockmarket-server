
<a name="definitions"></a>
## 定义

<a name="91299b03037c15057d9679d73ef42857"></a>
### 公共父类实体类
所有实体类的公共父类


|名称|说明|类型|
|---|---|---|
|**createTime**  <br>*可选*|用户创建时间|string|
|**id**  <br>*可选*|用户ID|integer (int64)|
|**updateTime**  <br>*可选*|用户更新时间|string|


<a name="54b45f79d3fdf41d46ba93346df5e481"></a>
### 响应信息类
返回给前端的响应信息类


|名称|说明|类型|
|---|---|---|
|**responseCode**  <br>*可选*|错误码|string|
|**responseData**  <br>*可选*|响应数据|object|
|**responseMessage**  <br>*可选*|响应消息|string|
|**statusCode**  <br>*可选*|HTTP状态码|integer (int32)|


<a name="00c7c47c76a9ae0888fcb30a58a91cb4"></a>
### 涨跌幅实体类
涨跌幅信息描述类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|证券代码|string|
|**codeName**  <br>*可选*|证券名称|string|
|**industry**  <br>*可选*|所属行业|string|
|**pctChg**  <br>*可选*|涨跌幅(百分比)|string|


<a name="c2d667b8a3f2d3260c2fffc9b5454834"></a>
### 用户实体类
用户信息描述类


|名称|说明|类型|
|---|---|---|
|**createTime**  <br>*可选*|用户创建时间|string|
|**email**  <br>*可选*|用户邮箱|string|
|**id**  <br>*可选*|用户ID|integer (int64)|
|**lastLoginTime**  <br>*可选*|用户上次登录时间|string|
|**name**  <br>*可选*|用户名字  <br>**长度** : `4 - 12`|string|
|**password**  <br>*可选*|用户密码|string|
|**phoneNumber**  <br>*可选*|用户手机号码  <br>**长度** : `11`|string|
|**role**  <br>*可选*|用户角色(0普通用户/1管理员/2超级管理员)|integer (int32)|
|**salt**  <br>*可选*|用户密码加密盐值  <br>**长度** : `8`|string|
|**updateTime**  <br>*可选*|用户更新时间|string|


<a name="e8b9b7c8da3592b2e6c6cb2dd180366b"></a>
### 行业分类实体类
行业分类信息描述类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|证券代码|string|
|**codeName**  <br>*可选*|证券名称|string|
|**industry**  <br>*可选*|所属行业|string|
|**industryClassification**  <br>*可选*|所属行业类别|string|
|**updateDate**  <br>*可选*|更新日期|string|


<a name="4998e7e76311eb24f89bc35726c32174"></a>
### 证券基本资料实体类
证券基本资料信息描述类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|证券代码|string|
|**codeName**  <br>*可选*|证券名称|string|
|**ipoDate**  <br>*可选*|上市日期|string|
|**outDate**  <br>*可选*|退市日期|string|
|**status**  <br>*可选*|上市状态, 其中1: 上市, 0: 退市|string|
|**type**  <br>*可选*|证券类型, 其中1: 股票, 2: 指数, 3: 其它|string|


<a name="a78b933b7c5f1a13461c04f4ef66ffaa"></a>
### 证券基本资料行业分类实体类
证券基本资料行业分类信息描述类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|证券代码|string|
|**codeName**  <br>*可选*|证券名称|string|
|**industry**  <br>*可选*|所属行业|string|
|**industryClassification**  <br>*可选*|所属行业类别|string|
|**ipoDate**  <br>*可选*|上市日期|string|
|**outDate**  <br>*可选*|退市日期|string|
|**status**  <br>*可选*|上市状态, 其中1: 上市, 0: 退市|string|
|**type**  <br>*可选*|证券类型, 其中1: 股票, 2: 指数, 3: 其它|string|
|**updateDate**  <br>*可选*|更新日期|string|



