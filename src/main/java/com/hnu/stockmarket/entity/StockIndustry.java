package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 行业分类实体类
 *
 * @author hnulht
 * @since 2021-07-12
 */
@ApiModel(value = "行业分类实体类", description = "行业分类信息描述类")
@Slf4j
@Data
public class StockIndustry implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "更新日期")
    @TableField(value = "updateDate", updateStrategy = FieldStrategy.IGNORED)
    private String updateDate;

    @ApiModelProperty(value = "证券代码")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String code;

    @ApiModelProperty(value = "证券名称")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String codeName;

    @ApiModelProperty(value = "所属行业")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String industry;

    @ApiModelProperty(value = "所属行业类别")
    @TableField(value = "industryClassification", updateStrategy = FieldStrategy.IGNORED)
    private String industryClassification;

}