package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 公共父类实体类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@ApiModel(value = "公共父类实体类", description = "所有实体类的公共父类")
@Slf4j
@Data
public class BaseEntity implements Serializable {

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.AUTO) // 主键注解, 其中主键字段名是id, 主键类型是数据库ID自增
    private Long id;

    @ApiModelProperty(value = "用户创建时间")
    @TableField(fill = FieldFill.INSERT) // 字段注解(非主键), 字段自动填充策略, fill值为INSERT时填充字段
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "用户更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE) // 字段注解(非主键), 字段自动填充策略, fill值为INSERT_UPDATE表示插入和更新时填充字段
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

}