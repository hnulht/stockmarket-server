package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 用户实体类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@ApiModel(value = "用户实体类", description = "用户信息描述类")
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名字")
    @NotBlank(message = "用户名字不能为空")
    @Size(min = 4, max = 12, message = "用户名字长度介于4到12个字符之间")
    private String name;

    @ApiModelProperty(value = "用户密码")
    @NotBlank(message = "用户密码不能为空")
    private String password;

    @ApiModelProperty(value = "用户角色(0普通用户/1管理员/2超级管理员)")
    private int role;

    @ApiModelProperty(value = "用户邮箱")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @Email(message = "用户邮箱格式不正确")
    private String email;

    @ApiModelProperty(value = "用户手机号码")
    @Size(min = 11, max = 11, message = "用户手机号码长度为11个字符")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String phoneNumber;

    @ApiModelProperty(value = "用户密码加密盐值")
    @Size(min = 8, max = 8, message = "用户密码加密盐值长度为8个字符")
    private String salt;

    @ApiModelProperty(value = "用户上次登录时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

}