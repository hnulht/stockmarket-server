package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 涨跌幅实体类
 *
 * @author hnulht
 * @since 2021-07-13
 */
@ApiModel(value = "涨跌幅实体类", description = "涨跌幅信息描述类")
@Slf4j
@Data
public class StockChg implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "证券代码")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String code;

    @ApiModelProperty(value = "证券名称")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String codeName;

    @ApiModelProperty(value = "涨跌幅(百分比)")
    @TableField(value = "pctChg", updateStrategy = FieldStrategy.IGNORED)
    private String pctChg;

    @ApiModelProperty(value = "所属行业")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String industry;

}