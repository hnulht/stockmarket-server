package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 股票指数数据实体类
 *
 * @author hnulht
 * @since 2021-07-14
 */
@ApiModel(value = "股票指数数据实体类", description = "股票指数数据信息描述类")
@Slf4j
@Data
public class StockIndex {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "证券代码")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String code;

    @ApiModelProperty(value = "证券名称")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String codeName;

    @ApiModelProperty(value = "涨跌幅(百分比)")
    @TableField(value = "pctChg", updateStrategy = FieldStrategy.IGNORED)
    private String pctChg;

    @ApiModelProperty(value = "今收盘价")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String close;

    @ApiModelProperty(value = "昨日收盘价")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String preclose;

}