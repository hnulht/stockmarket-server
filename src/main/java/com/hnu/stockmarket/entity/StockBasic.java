package com.hnu.stockmarket.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 证券基本资料实体类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@ApiModel(value = "证券基本资料实体类", description = "证券基本资料信息描述类")
@Slf4j
@Data
public class StockBasic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "证券代码")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String code;

    @ApiModelProperty(value = "证券名称")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String codeName;

    @ApiModelProperty(value = "上市日期")
    @TableField(value = "ipoDate", updateStrategy = FieldStrategy.IGNORED)
    private String ipoDate;

    @ApiModelProperty(value = "退市日期")
    @TableField(value = "outDate", updateStrategy = FieldStrategy.IGNORED)
    private String outDate;

    @ApiModelProperty(value = "证券类型, 其中1: 股票, 2: 指数, 3: 其它")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String type;

    @ApiModelProperty(value = "上市状态, 其中1: 上市, 0: 退市")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String status;

}