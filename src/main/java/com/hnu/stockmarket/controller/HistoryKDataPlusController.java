package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.hnu.stockmarket.common.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.hnu.stockmarket.constant.FlaskConstants.FLASK_BASE_URL;

/**
 * 历史A股K线数据前端控制器类
 *
 * @author hnulht
 * @since 2021-07-12
 */
@Api(tags = "历史A股K线数据接口")
@Slf4j
@RestController
@RequestMapping("/history_k_data_plus")
public class HistoryKDataPlusController extends BaseController {

    /**
     * 获取指定的历史A股K线数据信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "获取指定的历史A股K线数据", notes = "获取指定的历史A股K线数据信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/query_history_k_data_plus")
    public RestResponse updateHistoryKDataPlus(@RequestBody JSONObject jsonObject) {

        log.info("POST /history_k_data_plus/query_history_k_data_plus");
        log.info(jsonObject.toJSONString());

        String flaskURL = FLASK_BASE_URL + "/query_history_k_data_plus";
        JSONObject jsonObjectResponse = restTemplate.postForEntity(flaskURL, jsonObject, JSONObject.class).getBody();
        try {
            if (jsonObjectResponse != null) {

                log.info(jsonObjectResponse.toJSONString());

                if (Integer.parseInt(jsonObjectResponse.getString("statusCode")) == 200) {
                    JSONObject responseJSONData = (JSONObject) JSONObject.parse(jsonObjectResponse.getString("responseData"));
                    return RestResponse.succeed(responseJSONData);
                } else {
                    return RestResponse.fail(500, "B0001", "系统执行出错", "查询失败");
                }
            } else {
                return RestResponse.fail(500, "B0001", "系统执行出错", "查询失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}