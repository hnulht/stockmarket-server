package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.entity.StockBasic;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.hnu.stockmarket.constant.FlaskConstants.FLASK_BASE_URL;

/**
 * 证券基本资料前端控制器类
 *
 * @author hnulht
 * @since 2021-07-11
 */
@Api(tags = "证券基本资料数据接口")
@Slf4j
@RestController
@RequestMapping("/stock_basic")
public class StockBasicController extends BaseController {

    /**
     * 定时更新所有证券基本资料信息, 管理员也可以手动更新
     *
     * @return RestResponse
     */
    @ApiOperation(value = "更新证券基本资料", notes = "更新所有证券基本资料信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @GetMapping("/update_stock_basic")
    public RestResponse updateStockBasic() {

        log.info("GET /admin/update_stock_basic");

        String flaskURL = FLASK_BASE_URL + "/query_stock_basic";
        JSONObject jsonObjectResponse = restTemplate.getForEntity(flaskURL, JSONObject.class).getBody();
        try {
            if (jsonObjectResponse != null) {

                log.info(jsonObjectResponse.toJSONString());

                if (Integer.parseInt(jsonObjectResponse.getString("statusCode")) == 200) {
                    return RestResponse.succeed("更新成功");
                } else {
                    return RestResponse.fail(500, "B0001", "系统执行出错", "更新失败");
                }
            } else {
                return RestResponse.fail(500, "B0001", "系统执行出错", "更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 分页查询所有证券基本资料信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "分页查询所有证券基本资料", notes = "分页查询所有证券基本资料信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list")
    public RestResponse listAllStockBasic(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_basic/list");
        log.info(jsonObject.toJSONString());

        long pageNumber = Long.parseLong(jsonObject.getString("pageNumber"));
        long pageSize = Long.parseLong(jsonObject.getString("pageSize"));
        try {
            IPage<StockBasic> stockBasicIPage = new Page<>(pageNumber, pageSize);
            return RestResponse.succeed(stockBasicService.page(stockBasicIPage));
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}