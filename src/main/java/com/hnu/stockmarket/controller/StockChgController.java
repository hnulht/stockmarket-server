package com.hnu.stockmarket.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.entity.StockChg;
import com.hnu.stockmarket.utility.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.hnu.stockmarket.constant.FlaskConstants.FLASK_BASE_URL;

/**
 * 涨跌幅前端控制器类
 *
 * @author hnulht
 * @since 2021-07-13
 */
@Api(tags = "涨跌幅数据接口")
@Slf4j
@RestController
@RequestMapping("/stock_chg")
public class StockChgController extends BaseController {

    /**
     * 定时更新涨跌幅信息, 管理员也可以手动更新
     *
     * @return RestResponse
     */
    @ApiOperation(value = "更新涨跌幅", notes = "更新涨跌幅信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @GetMapping("/update_stock_chg")
    public RestResponse updateStockChg() {

        log.info("GET /stock_chg/update_stock_chg");

        ArrayList<StockChg> stockChgList = (ArrayList<StockChg>) stockChgService.list();
        String flaskURL = FLASK_BASE_URL + "/query_history_k_data_plus";
        boolean flag = true;
        for (StockChg stockChg : stockChgList) {
            JSONObject jsonObjectRequest = new JSONObject();
            jsonObjectRequest.put("code", stockChg.getCode());
            jsonObjectRequest.put("fields", "pctChg");
            jsonObjectRequest.put("start", DateUtils.getYesterdayDate());
            jsonObjectRequest.put("end", DateUtils.getTodayDate());
            jsonObjectRequest.put("frequency", "d");

            log.info(jsonObjectRequest.toJSONString());

            // 返回的响应JSON对象
            JSONObject jsonObjectResponse = restTemplate.postForEntity(flaskURL, jsonObjectRequest, JSONObject.class).getBody();
            try {
                if (jsonObjectResponse != null) {

                    log.info(jsonObjectResponse.toJSONString());

                    if (Integer.parseInt(jsonObjectResponse.getString("statusCode")) == 200) {
                        // 解析出响应JSON对象中responseData的JSON对象
                        JSONObject jsonObjectResponseData = jsonObjectResponse.getJSONObject("responseData");
                        // 解析出响应JSON对象中responseData的JSON对象中data的JSON数组
                        JSONArray jsonArrayData = jsonObjectResponseData.getJSONArray("data");
                        // 解析出响应JSON对象中responseData的JSON对象中data的JSON数组中的第一个数组
                        JSONArray jsonArray = (JSONArray) jsonArrayData.get(0);
                        // 解析出响应JSON对象中responseData的JSON对象中data的JSON数组中的第一个数组中的第一个元素
                        String pctChg = jsonArray.get(0).toString();
                        // 构建更新后的涨跌幅
                        StockChg newStockChg = new StockChg();
                        newStockChg.setCode(stockChg.getCode());
                        newStockChg.setCodeName(stockChg.getCodeName());
                        newStockChg.setIndustry(stockChg.getIndustry());
                        newStockChg.setPctChg(pctChg);
                        Wrapper<StockChg> stockChgWrapper = new QueryWrapper<StockChg>().eq("code", stockChg.getCode());
                        stockChgService.update(newStockChg, stockChgWrapper);
                    } else {
                        flag = false;
                    }
                } else {
                    flag = false;
                }
            } catch (Exception e) {
                return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
            }
        }
        if (flag) {
            return RestResponse.succeed("更新成功");
        } else {
            return RestResponse.fail(500, "B0001", "系统执行出错", "更新失败");
        }
    }

    /**
     * 分页查询涨幅榜
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "分页查询涨幅榜", notes = "分页查询所有涨幅榜信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list_rise")
    public RestResponse listRiseStockChg(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_chg/list_rise");
        log.info(jsonObject.toJSONString());

        long pageNumber = Long.parseLong(jsonObject.getString("pageNumber"));
        long pageSize = Long.parseLong(jsonObject.getString("pageSize"));
        try {
            Wrapper<StockChg> riseChgWrapper = new QueryWrapper<StockChg>().notLike("pctChg", "-%").orderByDesc("pctChg");
            IPage<StockChg> stockChgIPage = new Page<>(pageNumber, pageSize);
            return RestResponse.succeed(stockChgService.page(stockChgIPage, riseChgWrapper));
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 分页查询跌幅榜
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "分页查询跌幅榜", notes = "分页查询所有跌幅榜信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list_fall")
    public RestResponse listFallStockChg(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_chg/list_fall");
        log.info(jsonObject.toJSONString());

        long pageNumber = Long.parseLong(jsonObject.getString("pageNumber"));
        long pageSize = Long.parseLong(jsonObject.getString("pageSize"));
        try {
            Wrapper<StockChg> fallChgWrapper = new QueryWrapper<StockChg>().like("pctChg", "-%").orderByDesc("pctChg");
            IPage<StockChg> stockChgIPage = new Page<>(pageNumber, pageSize);
            return RestResponse.succeed(stockChgService.page(stockChgIPage, fallChgWrapper));
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据用户输入的信息查询涨跌幅信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "根据用户输入的信息查询涨跌幅", notes = "根据用户输入的信息查询涨跌幅信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list_query")
    public RestResponse listQueryStockChg(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_chg/list_query");
        log.info(jsonObject.toJSONString());

        String queryInput = jsonObject.getString("queryInput");
        try {
            Wrapper<StockChg> stockChgWrapper = new QueryWrapper<StockChg>().like("code_name", queryInput).or().like("code", queryInput).or().like("industry", queryInput);
            List<StockChg> stockChgList = stockChgService.list(stockChgWrapper);
            JSONObject jsonObjectResponse = new JSONObject();
            // 模糊查询只截取前30条结果
            if (stockChgList.size() > 30) {
                jsonObjectResponse.put("data", stockChgList.subList(0, 30));
                jsonObjectResponse.put("flag", 0);
                return RestResponse.succeed(jsonObjectResponse);
            } else {
                jsonObjectResponse.put("data", stockChgList);
                jsonObjectResponse.put("flag", 1);
                return RestResponse.succeed(jsonObjectResponse);
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}