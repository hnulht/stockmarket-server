package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.hnu.stockmarket.common.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.hnu.stockmarket.constant.FlaskConstants.FLASK_BASE_URL;

/**
 * 股票指数数据前端控制器类
 *
 * @author hnulht
 * @since 2021-07-14
 */
@Api(tags = "股票指数数据接口")
@Slf4j
@RestController
@RequestMapping("/stock_index")
public class StockIndexController extends BaseController {

    /**
     * 定时更新股票指数数据信息, 管理员也可以手动更新
     *
     * @return RestResponse
     */
    @ApiOperation(value = "更新股票指数数据", notes = "更新股票指数数据信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @GetMapping("/update_stock_index")
    public RestResponse updateStockIndex() {

        log.info("GET /stock_index/update_stock_index");

        String flaskURL = FLASK_BASE_URL + "/query_stock_index";
        JSONObject jsonObjectResponse = restTemplate.getForEntity(flaskURL, JSONObject.class).getBody();
        try {
            if (jsonObjectResponse != null) {

                log.info(jsonObjectResponse.toJSONString());

                if (Integer.parseInt(jsonObjectResponse.getString("statusCode")) == 200) {
                    return RestResponse.succeed("更新成功");
                } else {
                    return RestResponse.fail(500, "B0001", "系统执行出错", "更新失败");
                }
            } else {
                return RestResponse.fail(500, "B0001", "系统执行出错", "更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 查询所有股票指数数据信息
     *
     * @return RestResponse
     */
    @ApiOperation(value = "查询所有股票指数数据", notes = "查询所有股票指数数据信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @GetMapping("/list_stock_index")
    public RestResponse listStockIndex() {

        log.info("GET /stock_index/list_stock_index");

        try {
            return RestResponse.succeed(stockIndexService.list());
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}