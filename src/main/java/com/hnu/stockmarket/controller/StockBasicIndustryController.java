package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.entity.StockBasicIndustry;
import com.hnu.stockmarket.entity.StockChg;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 证券基本资料行业分类前端控制器类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Api(tags = "证券基本资料行业分类数据接口")
@Slf4j
@RestController
@RequestMapping("/stock_basic_industry")
public class StockBasicIndustryController extends BaseController {

    /**
     * 分页查询所有证券基本资料行业分类信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "分页查询所有证券基本资料行业分类", notes = "分页查询所有证券基本资料行业分类信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list")
    public RestResponse listAllStockBasicIndustry(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_basic_industry/list");
        log.info(jsonObject.toJSONString());

        long pageNumber = Long.parseLong(jsonObject.getString("pageNumber"));
        long pageSize = Long.parseLong(jsonObject.getString("pageSize"));
        try {
            IPage<StockBasicIndustry> stockBasicIndustryIPage = new Page<>(pageNumber, pageSize);
            return RestResponse.succeed(stockBasicIndustryService.listByPage(stockBasicIndustryIPage));
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据用户输入的信息查询证券基本资料行业分类信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "根据用户输入的信息查询证券基本资料行业分类", notes = "根据用户输入的信息查询证券基本资料行业分类信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list_query")
    public RestResponse listQueryStockBasicIndustry(@RequestBody JSONObject jsonObject) {

        log.info("POST /stock_basic_industry/list_query");
        log.info(jsonObject.toJSONString());

        String queryInput = jsonObject.getString("queryInput");
        try {
            List<StockBasicIndustry> stockBasicIndustryList = stockBasicIndustryService.listQuery(queryInput);
            JSONObject jsonObjectResponse = new JSONObject();
            // 模糊查询只截取前30条结果
            if (stockBasicIndustryList.size() > 30) {
                jsonObjectResponse.put("data", stockBasicIndustryList.subList(0, 30));
                jsonObjectResponse.put("flag", 0);
                return RestResponse.succeed(jsonObjectResponse);
            } else {
                jsonObjectResponse.put("data", stockBasicIndustryList);
                jsonObjectResponse.put("flag", 1);
                return RestResponse.succeed(jsonObjectResponse);
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}