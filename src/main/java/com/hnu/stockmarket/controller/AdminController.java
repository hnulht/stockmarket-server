package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.entity.User;
import com.hnu.stockmarket.utility.CookieUtils;
import com.hnu.stockmarket.utility.HashUtils;
import com.hnu.stockmarket.utility.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * 管理员前端控制器类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Api(tags = "管理员数据接口")
@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController extends BaseController {

    /**
     * 验证管理员或用户浏览页面的权限
     *
     * @return RestResponse
     */
    @ApiOperation(value = "验证权限", notes = "验证管理员或用户浏览页面的权限")
    @ApiResponses({
            @ApiResponse(code = 200, message = "验证成功"),
            @ApiResponse(code = 500, message = "服务器内部错误")
    })
    @PostMapping("/verify")
    public RestResponse verifyPermission() {

        log.info("POST /admin/verify");

        try {
            return RestResponse.succeed("验证成功");
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 查询总用户数
     *
     * @return RestResponse
     */
    @ApiOperation(value = "查询总用户数", notes = "查询总用户数")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "服务器内部错误")
    })
    @GetMapping("/count")
    public RestResponse countAllUsers() {

        log.info("GET /admin/count");

        try {
            return RestResponse.succeed(userService.count());
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据管理员输入的信息检验登录
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "管理员登录", notes = "根据管理员输入的信息检验登录")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功"),
            @ApiResponse(code = 401, message = "权限不足"),
            @ApiResponse(code = 403, message = "密码不正确"),
            @ApiResponse(code = 404, message = "用户未注册"),
            @ApiResponse(code = 500, message = "服务器内部错误")
    })
    @PostMapping("/login")
    public RestResponse login(@RequestBody JSONObject jsonObject, HttpServletResponse response) {

        log.info("POST /admin/login");
        log.info(jsonObject.toJSONString());

        String name = jsonObject.getString("name");
        String password = jsonObject.getString("password");
        try {
            User user = userService.getOne(new QueryWrapper<User>().eq("name", name));
            if (user != null) {
                if (!user.getPassword().equals(HashUtils.hashPassword(password, user.getSalt()))) {
                    return RestResponse.fail(403, "A0120", "密码校验失败", "密码不正确");
                }
                if (user.getRole() == 0) {
                    return RestResponse.fail(401, "A0300", "访问权限异常", "权限不足");
                }
                String token = JwtUtils.generateToken(user);
                response.addCookie(CookieUtils.buildCookie("token", token));
                if (token != null) {

                    log.info("TOKEN " + token);

                    user.setLastLoginTime(LocalDateTime.now());
                    userService.updateById(user);
                    return RestResponse.succeed(token);
                }
                return RestResponse.fail(500, "B0001", "系统执行出错", "Token生成失败");
            } else {
                return RestResponse.fail(404, "A0201", "用户账户不存在", "用户未注册");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 插入用户信息
     *
     * @param user user
     * @return RestResponse
     */
    @ApiOperation(value = "插入用户", notes = "插入用户信息, 传入待插入的用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "插入成功"),
            @ApiResponse(code = 500, message = "插入失败/服务器内部错误")
    })
    @PostMapping("/save")
    public RestResponse saveUser(@Validated @RequestBody User user) {

        log.info("POST /admin/save");
        log.info(user.toString());

        try {
            if (userService.save(user)) {
                return RestResponse.succeed("插入成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "插入失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据id删除用户信息
     *
     * @param id id
     * @return RestResponse
     */
    @ApiOperation(value = "删除用户", notes = "根据id删除用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除成功"),
            @ApiResponse(code = 500, message = "删除失败/服务器内部错误")
    })
    @DeleteMapping("/remove/{id}")
    public RestResponse removeUserById(@PathVariable("id") Long id) {
        try {
            if (userService.removeById(id)) {
                return RestResponse.succeed("删除成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "删除失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据id更新用户信息
     *
     * @param user user
     * @return RestResponse
     */
    @ApiOperation(value = "更新用户", notes = "根据id更新用户信息, 传入待更新的用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @PutMapping("/update")
    public RestResponse updateUserById(@Validated @RequestBody User user) {
        log.info(user.toString());
        try {
            if (userService.updateById(user)) {
                return RestResponse.succeed("更新成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 分页查询所有用户信息
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "分页查询所有用户", notes = "分页查询所有用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/list")
    public RestResponse listAllUsers(@RequestBody JSONObject jsonObject) {
        long pageNumber = Long.parseLong(jsonObject.getString("pageNumber"));
        long pageSize = Long.parseLong(jsonObject.getString("pageSize"));
        try {
            IPage<User> userIPage = new Page<>(pageNumber, pageSize);
            return RestResponse.succeed(userService.page(userIPage));
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据id查询用户信息
     *
     * @param id id
     * @return RestResponse
     */
    @ApiOperation(value = "查询用户", notes = "根据id查询用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @PostMapping("/get/{id}")
    public RestResponse getUserById(@PathVariable("id") Long id) {
        try {
            User user = userService.getById(id);
            if (user != null) {
                return RestResponse.succeed(user);
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "查询失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}