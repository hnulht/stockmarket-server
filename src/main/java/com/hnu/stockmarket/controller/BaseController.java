package com.hnu.stockmarket.controller;

import com.hnu.stockmarket.entity.*;
import com.hnu.stockmarket.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 公共父类前端控制器类
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Api(tags = "公共数据接口")
@Slf4j
@RestController
public class BaseController {

    @Autowired
    public UserService userService;

    @Autowired
    public RestTemplate restTemplate;

    @Autowired
    public StockBasicService stockBasicService;

    @Autowired
    public StockIndustryService stockIndustryService;

    @Autowired
    public StockBasicIndustryService stockBasicIndustryService;

    @Autowired
    public StockIndexService stockIndexService;

    @Autowired
    public StockChgService stockChgService;

    /**
     * Swagger2实体类的models必须在控制器中加注解@RequestBody才能显示
     *
     * @param baseEntity         baseEntity
     * @param stockBasic         stockBasic
     * @param stockBasicIndustry stockBasicIndustry
     * @param stockChg           stockChg
     * @param stockIndustry      stockIndustry
     */
    @ApiOperation(value = "Swagger添加模型", notes = "Swagger添加模型")
    @PostMapping("/swagger")
    public void swaggerModel(@RequestBody BaseEntity baseEntity,
                             @RequestBody StockBasic stockBasic,
                             @RequestBody StockBasicIndustry stockBasicIndustry,
                             @RequestBody StockChg stockChg,
                             @RequestBody StockIndustry stockIndustry) {
    }

}