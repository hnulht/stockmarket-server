package com.hnu.stockmarket.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.entity.User;
import com.hnu.stockmarket.utility.CookieUtils;
import com.hnu.stockmarket.utility.HashUtils;
import com.hnu.stockmarket.utility.JwtUtils;
import com.hnu.stockmarket.utility.RandUtils;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * 用户前端控制器类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Api(tags = "用户数据接口")
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    /**
     * 根据Token获取用户信息
     *
     * @param request request
     * @return RestResponse
     */
    @ApiOperation(value = "获取用户", notes = "根据Token获取用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取成功"),
            @ApiResponse(code = 400, message = "获取失败"),
            @ApiResponse(code = 500, message = "查询失败/服务器内部错误")
    })
    @GetMapping("/get")
    public RestResponse getUserByToken(HttpServletRequest request) {

        log.info("GET /user/get");

        try {
            String token = CookieUtils.findCookie(request.getCookies(), "token").getValue();
            if (token != null) {

                log.info("TOKEN " + token);

                // 解析Token
                Claims claims = JwtUtils.getClaimByToken(token);
                if (claims == null) {
                    // Token被篡改导致校验失败
                    return RestResponse.fail("token无效, 请重新登录");
                } else if (JwtUtils.isTokenExpired(claims)) {
                    // Token过期导致检测失败
                    return RestResponse.fail("token无效, 请重新登录");
                }
                Long id = (long) (int) claims.get("id");
                User user = userService.getById(id);
                if (user != null) {
                    return RestResponse.succeed(user);
                } else {
                    return RestResponse.fail(500, "C0300", "数据库服务出错", "查询失败");
                }
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
        return RestResponse.fail(500, "B0001", "系统执行出错", "未知错误");
    }

    /**
     * 根据用户输入的信息注册用户
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "用户注册", notes = "根据用户输入的信息注册用户")
    @ApiResponses({
            @ApiResponse(code = 200, message = "注册成功"),
            @ApiResponse(code = 403, message = "用户名已存在"),
            @ApiResponse(code = 500, message = "服务器内部错误")
    })
    @PostMapping("/register")
    public RestResponse register(@RequestBody JSONObject jsonObject) {

        log.info("POST /user/register");
        log.info(jsonObject.toJSONString());

        // 构建更新后的用户实体
        String name = jsonObject.getString("name");
        String password = jsonObject.getString("password");
        String email = jsonObject.getString("email");
        String phoneNumber = jsonObject.getString("phoneNumber");
        String salt = RandUtils.getSalt();
        User newUser = new User();
        newUser.setName(name);
        newUser.setPassword(HashUtils.hashPassword(password, salt));
        newUser.setRole(0);
        newUser.setEmail(email);
        newUser.setPhoneNumber(phoneNumber);
        newUser.setSalt(salt);
        newUser.setLastLoginTime(null);
        try {
            // 检测输入的用户名是否已经存在
            for (User user : userService.list()) {
                if (newUser.getName().equals(user.getName())) {
                    return RestResponse.fail(403, "A0111", "用户名已存在", "用户名已存在");
                }
            }
            if (userService.save(newUser)) {
                return RestResponse.succeed("注册成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "注册失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 用户/管理员退出登录
     *
     * @param response response
     * @return RestResponse
     */
    @ApiOperation(value = "用户退出登录", notes = "用户/管理员退出登录")
    @ApiResponses({
            @ApiResponse(code = 200, message = "退出登录成功"),
            @ApiResponse(code = 500, message = "退出登录失败/服务器内部错误")
    })
    @PostMapping("/logout")
    public RestResponse logout(HttpServletResponse response) {

        log.info("POST /user/logout");

        try {
            response.addCookie(CookieUtils.buildCookie("token", null));
            return RestResponse.succeed("退出登录成功");
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据用户输入的信息检验登录
     *
     * @param jsonObject jsonObject
     * @return RestResponse
     */
    @ApiOperation(value = "用户登录", notes = "根据用户输入的信息检验登录")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功"),
            @ApiResponse(code = 403, message = "密码不正确"),
            @ApiResponse(code = 404, message = "用户未注册"),
            @ApiResponse(code = 500, message = "服务器内部错误")
    })
    @PostMapping("/login")
    public RestResponse login(@RequestBody JSONObject jsonObject) {

        log.info("POST /user/login");
        log.info(jsonObject.toJSONString());

        String name = jsonObject.getString("name");
        String password = jsonObject.getString("password");
        try {
            User user = userService.getOne(new QueryWrapper<User>().eq("name", name));
            if (user != null) {
                if (!user.getPassword().equals(HashUtils.hashPassword(password, user.getSalt()))) {
                    return RestResponse.fail(403, "A0120", "密码校验失败", "密码不正确");
                }
                user.setLastLoginTime(LocalDateTime.now());
                userService.updateById(user);
                return RestResponse.succeed(user);
            } else {
                return RestResponse.fail(404, "A0201", "用户账户不存在", "用户未注册");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据id更新用户信息
     *
     * @param user user
     * @return RestResponse
     */
    @ApiOperation(value = "更新用户", notes = "根据id更新用户信息, 传入待更新的用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @PutMapping("/update")
    public RestResponse updateUserById(@Validated @RequestBody User user) {

        log.info("PUT /user/update");

        try {
            if (userService.updateById(user)) {
                return RestResponse.succeed("更新成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

    /**
     * 根据id更新更新用户密码
     *
     * @param user user
     * @return RestResponse
     */
    @ApiOperation(value = "更新用户密码", notes = "根据id更新更新用户密码, 传入待更新的用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "更新成功"),
            @ApiResponse(code = 500, message = "更新失败/服务器内部错误")
    })
    @PutMapping("/updateUserPassword")
    public RestResponse updateUserPassword(@Validated @RequestBody User user) {

        log.info("PUT /user/updateUserPassword");

        String password = user.getPassword();
        String salt = user.getSalt();
        user.setPassword(HashUtils.hashPassword(password, salt));
        try {
            if (userService.updateById(user)) {
                return RestResponse.succeed("更新成功");
            } else {
                return RestResponse.fail(500, "C0300", "数据库服务出错", "更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail(500, "B0001", "系统执行出错", e.getMessage());
        }
    }

}