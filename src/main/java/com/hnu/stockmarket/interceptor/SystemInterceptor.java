package com.hnu.stockmarket.interceptor;

import com.google.gson.Gson;
import com.hnu.stockmarket.common.RestResponse;
import com.hnu.stockmarket.constant.SystemConstants;
import com.hnu.stockmarket.utility.CookieUtils;
import com.hnu.stockmarket.utility.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 系统接口拦截器
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
@Component
public class SystemInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();

        log.info("拦截器捕获的请求接口" + requestURI);
        log.info("系统中无需拦截的请求接口" + SystemConstants.INTERCEPT_IGNORE_URI_LIST.toString());

        // 登录和注册接口不需要进行Token拦截和校验
        for (String interceptIgnoreUri : SystemConstants.INTERCEPT_IGNORE_URI_LIST) {
            if (interceptIgnoreUri.equals(requestURI)) {
                return true;
            }
        }

        // 获取Cookie中的Token值
        try {
            // 获取请求参数(RequestParam)中的Token值
            //String token = request.getParameter("token");
            String token = CookieUtils.findCookie(request.getCookies(), "token").getValue();
            if (token != null) {
                // 解析Token
                Claims claims = JwtUtils.getClaimByToken(token);
                if (claims == null) {
                    // Token被篡改导致校验失败
                    sendJsonMessage(response, RestResponse.fail("token无效, 请重新登录"));
                    return false;
                } else if (JwtUtils.isTokenExpired(claims)) {
                    // Token过期导致检测失败
                    sendJsonMessage(response, RestResponse.fail("token过期, 请重新登录"));
                    return false;
                }

                log.info("请求接口token中的用户id=" + claims.get("id"));
                log.info("请求接口token中的用户id的类型=" + claims.get("id").getClass());
                log.info("请求接口token中的用户name=" + claims.get("name"));
                log.info("请求接口token中的用户name的类型=" + claims.get("name").getClass());

                Long id = (long) (int) claims.get("id");
                String name = (String) claims.get("name");
                // 把这两个参数放到请求中, 可以在controller中获取到, 不需要在controller中在用Jwt解密, request.getAttribute("属性名")即可获取
                request.setAttribute("id", id);
                request.setAttribute("name", name);
                return true;
            }
        } catch (Exception e) {
            sendJsonMessage(response, RestResponse.fail("token为空, 请先登录"));
        }
        return false;
    }

    /**
     * 给前端返回响应信息
     *
     * @param response response
     * @param obj      obj
     * @throws Exception Exception
     */
    public static void sendJsonMessage(HttpServletResponse response, Object obj) throws Exception {
        Gson g = new Gson();
        response.setContentType("application/json; charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.print(g.toJson(obj));
        writer.close();
        response.flushBuffer();
    }

}