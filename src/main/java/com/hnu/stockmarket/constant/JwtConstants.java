package com.hnu.stockmarket.constant;

import lombok.extern.slf4j.Slf4j;

/**
 * jwt相关常量
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
public class JwtConstants {

    // Token主题
    public static final String SUBJECT = "stockmarket";

    // 加密秘钥
    public static final String SECRET = "ji8n3439n439n43ld9ne9343fdfer49h";

    // Token有效时长, 单位秒, 设置为7天
    public static final long EXPIRE = 604800;

    // Token首部
    public static final String HEADER = "Authorization";

}