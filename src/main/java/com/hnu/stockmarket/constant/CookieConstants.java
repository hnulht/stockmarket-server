package com.hnu.stockmarket.constant;

import lombok.extern.slf4j.Slf4j;

/**
 * Cookie相关常量
 *
 * @author hnulht
 * @since 2021-07-06
 */
@Slf4j
public class CookieConstants {

    public static final String DOMAIN = "10.69.42.205";

    public static final String PATH = "/";

    public static final int MAX_AGE = 604800;

}