package com.hnu.stockmarket.constant;

import lombok.extern.slf4j.Slf4j;

/**
 * Flask微服务端相关常量
 *
 * @author hnulht
 * @since 2021-07-10
 */
@Slf4j
public class FlaskConstants {

    // Flask host + port
    public static final String FLASK_BASE_URL = "http://127.0.0.1:5000";

}