package com.hnu.stockmarket.constant;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * 系统相关常量
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
public class SystemConstants {

    // System host + port
    public static final String SYSTEM_BASE_URL = "http://127.0.0.1:8081";

    // 系统忽略的请求接口
    public static final ArrayList<String> INTERCEPT_IGNORE_URI_LIST = new ArrayList<>();

    static {
        // 所有与admin接口相关的都会被拦截, 除了/admin/login
        INTERCEPT_IGNORE_URI_LIST.add("/admin/login");
    }

}