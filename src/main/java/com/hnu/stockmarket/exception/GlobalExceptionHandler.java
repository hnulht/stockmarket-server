package com.hnu.stockmarket.exception;

import com.hnu.stockmarket.common.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.file.AccessDeniedException;

/**
 * 全局控制器异常处理
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理访问权限不足的异常
     *
     * @param e e
     * @return response
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = AccessDeniedException.class)
    public RestResponse handler(AccessDeniedException e) {

        log.info("访问权限不足：---------------->{}", e.getMessage());

        return RestResponse.fail(403, "A0300", "访问权限不足", null);
    }

    /**
     * 处理实体校验的异常
     *
     * @param e e
     * @return response
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public RestResponse handler(MethodArgumentNotValidException e) {

        log.info("实体校验异常：---------------->{}", e.getMessage());

        BindingResult bindingResult = e.getBindingResult();
        ObjectError objectError = bindingResult.getAllErrors().stream().findFirst().get();
        return RestResponse.fail(objectError.getDefaultMessage());
    }

    /**
     * 处理Assert的异常
     *
     * @param e e
     * @return response
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = IllegalArgumentException.class)
    public RestResponse handler(IllegalArgumentException e) {

        log.error("Assert异常：---------------->{}", e.getMessage());

        return RestResponse.fail(e.getMessage());
    }

    /**
     * 处理运行时的异常
     *
     * @param e e
     * @return response
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = RuntimeException.class)
    public RestResponse handler(RuntimeException e) {

        log.error("运行时异常：---------------->{}", e.getMessage());

        return RestResponse.fail(e.getMessage());
    }

}