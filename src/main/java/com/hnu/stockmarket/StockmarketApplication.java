package com.hnu.stockmarket;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
@SpringBootApplication
@EnableAdminServer
public class StockmarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockmarketApplication.class, args);
    }

}