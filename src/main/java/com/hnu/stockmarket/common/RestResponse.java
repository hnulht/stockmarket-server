package com.hnu.stockmarket.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 返回给前端的响应信息封装
 * REST架构分格(Representational State Transfer)
 *
 * @author hnulht
 * @since 2021-07-04
 */
@ApiModel(value = "响应信息类", description = "返回给前端的响应信息类")
@Slf4j
@Data
public class RestResponse implements Serializable {

    @ApiModelProperty(value = "HTTP状态码")
    private int statusCode;

    @ApiModelProperty(value = "错误码")
    private String responseCode;

    @ApiModelProperty(value = "响应消息")
    private String responseMessage;

    @ApiModelProperty(value = "响应数据")
    private Object responseData;

    public RestResponse() {
    }

    /**
     * 请求成功完成, 不返回响应数据
     *
     * @return response
     */
    public static RestResponse succeed() {
        return succeed(200, "00000", "请求成功完成", null);
    }

    /**
     * 请求成功完成, 返回响应数据
     *
     * @param responseData responseData
     * @return response
     */
    public static RestResponse succeed(Object responseData) {
        return succeed(200, "00000", "请求成功完成", responseData);
    }

    /**
     * 请求成功完成, 自定义响应结果
     *
     * @param statusCode      statusCode
     * @param responseCode    responseCode
     * @param responseMessage responseMessage
     * @param responseData    responseData
     * @return response
     */
    public static RestResponse succeed(int statusCode, String responseCode, String responseMessage, Object responseData) {
        RestResponse response = new RestResponse();
        response.setStatusCode(statusCode);
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        response.setResponseData(responseData);
        return response;
    }

    /**
     * 请求失败, 不返回响应数据, 未知错误响应信息
     *
     * @return response
     */
    public static RestResponse fail() {
        return fail(400, "B0001", "未知错误", null);
    }

    /**
     * 请求失败, 不返回响应数据, 自定义响应信息
     *
     * @param responseMessage responseMessage
     * @return response
     */
    public static RestResponse fail(String responseMessage) {
        return fail(400, "B0001", responseMessage, null);
    }

    /**
     * 请求失败, 自定义响应结果
     *
     * @param statusCode      statusCode
     * @param responseCode    responseCode
     * @param responseMessage responseMessage
     * @param responseData    responseData
     * @return response
     */
    public static RestResponse fail(int statusCode, String responseCode, String responseMessage, Object responseData) {
        RestResponse response = new RestResponse();
        response.setStatusCode(statusCode);
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        response.setResponseData(responseData);
        return response;
    }

}