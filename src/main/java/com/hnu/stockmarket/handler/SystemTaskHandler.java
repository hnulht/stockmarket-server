package com.hnu.stockmarket.handler;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.hnu.stockmarket.constant.SystemConstants.SYSTEM_BASE_URL;

/**
 * 系统定时任务处理类
 *
 * @author hnulht
 * @since 2021-07-13
 */
@Slf4j
@Component
public class SystemTaskHandler {

    @Autowired
    public RestTemplate restTemplate;

    /**
     * 每天0点定时更新涨跌幅信息
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void scheduledUpdateStockChg() {
        log.info("start cron task ....");
        String SystemURL = SYSTEM_BASE_URL + "/stock_chg/update_stock_chg";
        restTemplate.getForEntity(SystemURL, JSONObject.class);
    }

    /**
     * 每天1点定时更新证券基本资料信息
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void scheduledUpdateStockBasic() {
        log.info("start cron task ....");
        String SystemURL = SYSTEM_BASE_URL + "/stock_basic/update_stock_basic";
        restTemplate.getForEntity(SystemURL, JSONObject.class);
    }

    /**
     * 每天2点定时更新行业分类信息
     */
    @Scheduled(cron = "0 0 2 * * ?")
    public void scheduledUpdateStockIndustry() {
        log.info("start cron task ....");
        String SystemURL = SYSTEM_BASE_URL + "/stock_industry/update_stock_industry";
        restTemplate.getForEntity(SystemURL, JSONObject.class);
    }

    /**
     * 每天3点定时更新股票指数数据信息
     */
    @Scheduled(cron = "0 0 3 * * ?")
    public void scheduledUpdateStockIndex() {
        log.info("start cron task ....");
        String SystemURL = SYSTEM_BASE_URL + "/stock_index/update_stock_index";
        restTemplate.getForEntity(SystemURL, JSONObject.class);
    }

}