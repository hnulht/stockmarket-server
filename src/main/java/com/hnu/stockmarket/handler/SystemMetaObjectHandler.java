package com.hnu.stockmarket.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 字段自动填充处理类
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
@Component
public class SystemMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入时的填充策略
     *
     * @param metaObject metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {

        log.info("start insert fill ....");

        this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

    /**
     * 更新时的填充策略
     *
     * @param metaObject metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {

        log.info("start update fill ....");

        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

}