package com.hnu.stockmarket.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 配置Swagger
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * 配置Docket
     *
     * @return Docket
     */
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hnu.stockmarket.controller")) // api方法配置要扫描的控制器位置
                .paths(PathSelectors.any()) // 配置路径
                .build()
                .apiInfo(
                        new ApiInfoBuilder()
                                .description("接口测试文档") // 描述
                                .contact(
                                        new Contact("hnulht", "https://gitee.com/hnulht/", "381874042@qq.com")
                                ) // 联系人信息
                                .version("v1.0") // 版本
                                .title("股票市场分析系统API测试文档") // 标题
                                .license("Apache2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                                .build()
                );
    }

}