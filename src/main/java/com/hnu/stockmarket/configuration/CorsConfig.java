package com.hnu.stockmarket.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置网关全局设置, 解决跨域问题
 * CORS(Cross-Origin Resource Sharing)跨域资源共享技术标准
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    /**
     * 页面跨域访问Controller过滤
     *
     * @param registry registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 添加映射路径
        registry.addMapping("/**") // 允许所有的当前站点的请求地址都支持跨域访问
                .allowedOriginPatterns("*") // 允许所有域名进行跨域调用
                .allowedHeaders("*") // 允许放行全部原始头信息
                .allowCredentials(true) // 允许跨越发送cookie
                .allowedMethods("GET", "HEAD", "POST", "DELETE", "PUT", "OPTIONS") // 允许所有请求方法跨域调用
                .maxAge(3600); // 超时时长设置为1小时, 时间单位是秒

    }

}