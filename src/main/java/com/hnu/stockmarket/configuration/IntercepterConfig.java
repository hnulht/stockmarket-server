package com.hnu.stockmarket.configuration;

import com.hnu.stockmarket.interceptor.SystemInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置拦截器
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
@Configuration
public class IntercepterConfig implements WebMvcConfigurer {

    @Autowired
    private SystemInterceptor systemInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截需要进行Token验证的所有接口请求, 只需拦截管理员的所有请求
        registry.addInterceptor(systemInterceptor)
                .addPathPatterns("/admin/**");
    }

}