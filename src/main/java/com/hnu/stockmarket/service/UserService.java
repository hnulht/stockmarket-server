package com.hnu.stockmarket.service;

import com.hnu.stockmarket.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户服务类
 *
 * @author hnulht
 * @since 2021-07-05
 */
public interface UserService extends IService<User> {

}