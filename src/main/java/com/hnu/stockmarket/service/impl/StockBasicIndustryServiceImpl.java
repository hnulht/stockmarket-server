package com.hnu.stockmarket.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.stockmarket.entity.StockBasicIndustry;
import com.hnu.stockmarket.mapper.StockBasicIndustryMapper;
import com.hnu.stockmarket.service.StockBasicIndustryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 证券基本资料行业分类服务实现类
 *
 * @author hnulht
 * @since 2021-07-12
 */
@Slf4j
@Service
public class StockBasicIndustryServiceImpl extends ServiceImpl<StockBasicIndustryMapper, StockBasicIndustry> implements StockBasicIndustryService {

    @Autowired
    private StockBasicIndustryMapper stockBasicIndustryMapper;

    @Override
    public IPage<StockBasicIndustry> listByPage(IPage<StockBasicIndustry> stockBasicIndustryIPage) {
        return stockBasicIndustryMapper.listByPage(stockBasicIndustryIPage);
    }

    @Override
    public List<StockBasicIndustry> listQuery(String queryInput) {
        return stockBasicIndustryMapper.listQuery(queryInput);
    }


}