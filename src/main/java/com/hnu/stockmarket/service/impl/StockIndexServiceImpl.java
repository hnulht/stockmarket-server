package com.hnu.stockmarket.service.impl;

import com.hnu.stockmarket.entity.StockIndex;
import com.hnu.stockmarket.mapper.StockIndexMapper;
import com.hnu.stockmarket.service.StockIndexService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 股票指数数据服务实现类
 *
 * @author hnulht
 * @since 2021-07-14
 */
@Slf4j
@Service
public class StockIndexServiceImpl extends ServiceImpl<StockIndexMapper, StockIndex> implements StockIndexService {

}