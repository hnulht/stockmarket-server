package com.hnu.stockmarket.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.stockmarket.entity.StockBasic;
import com.hnu.stockmarket.mapper.StockBasicMapper;
import com.hnu.stockmarket.service.StockBasicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 证券基本资料服务实现类
 *
 * @author hnulht
 * @since 2021-07-11
 */
@Slf4j
@Service
public class StockBasicServiceImpl extends ServiceImpl<StockBasicMapper, StockBasic> implements StockBasicService {

}