package com.hnu.stockmarket.service.impl;

import com.hnu.stockmarket.entity.StockChg;
import com.hnu.stockmarket.mapper.StockChgMapper;
import com.hnu.stockmarket.service.StockChgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 涨跌幅服务实现类
 *
 * @author hnulht
 * @since 2021-07-13
 */
@Slf4j
@Service
public class StockChgServiceImpl extends ServiceImpl<StockChgMapper, StockChg> implements StockChgService {

}