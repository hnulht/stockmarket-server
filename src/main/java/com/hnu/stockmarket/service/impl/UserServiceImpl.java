package com.hnu.stockmarket.service.impl;

import com.hnu.stockmarket.entity.User;
import com.hnu.stockmarket.mapper.UserMapper;
import com.hnu.stockmarket.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 用户服务实现类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}