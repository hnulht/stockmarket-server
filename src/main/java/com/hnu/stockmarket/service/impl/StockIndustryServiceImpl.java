package com.hnu.stockmarket.service.impl;

import com.hnu.stockmarket.entity.StockIndustry;
import com.hnu.stockmarket.mapper.StockIndustryMapper;
import com.hnu.stockmarket.service.StockIndustryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 行业分类服务实现类
 *
 * @author hnulht
 * @since 2021-07-11
 */
@Slf4j
@Service
public class StockIndustryServiceImpl extends ServiceImpl<StockIndustryMapper, StockIndustry> implements StockIndustryService {

}