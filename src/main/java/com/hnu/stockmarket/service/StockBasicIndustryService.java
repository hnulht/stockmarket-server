package com.hnu.stockmarket.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hnu.stockmarket.entity.StockBasicIndustry;

import java.util.List;

/**
 * 证券基本资料行业分类服务类
 *
 * @author hnulht
 * @since 2021-07-12
 */
public interface StockBasicIndustryService extends IService<StockBasicIndustry> {

    IPage<StockBasicIndustry> listByPage(IPage<StockBasicIndustry> stockBasicIndustryIPage);

    List<StockBasicIndustry> listQuery(String queryInput);

}