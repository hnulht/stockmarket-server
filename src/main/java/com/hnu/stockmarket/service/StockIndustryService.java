package com.hnu.stockmarket.service;

import com.hnu.stockmarket.entity.StockIndustry;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 行业分类服务类
 *
 * @author hnulht
 * @since 2021-07-11
 */
public interface StockIndustryService extends IService<StockIndustry> {

}