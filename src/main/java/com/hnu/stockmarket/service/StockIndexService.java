package com.hnu.stockmarket.service;

import com.hnu.stockmarket.entity.StockIndex;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 股票指数数据服务类
 *
 * @author hnulht
 * @since 2021-07-14
 */
public interface StockIndexService extends IService<StockIndex> {

}
