package com.hnu.stockmarket.service;

import com.hnu.stockmarket.entity.StockChg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 涨跌幅服务类
 *
 * @author hnulht
 * @since 2021-07-13
 */
public interface StockChgService extends IService<StockChg> {

}