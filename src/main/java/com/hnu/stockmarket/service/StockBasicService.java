package com.hnu.stockmarket.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnu.stockmarket.entity.StockBasic;

/**
 * 证券基本资料服务类
 *
 * @author hnulht
 * @since 2021-07-11
 */
public interface StockBasicService extends IService<StockBasic> {

}