package com.hnu.stockmarket.utility;

import com.hnu.stockmarket.constant.JwtConstants;
import com.hnu.stockmarket.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * jwt(json web token)工具类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
public class JwtUtils {

    /**
     * 生成jwt
     *
     * @param user user
     * @return jwt
     */
    public static String generateToken(User user) {
        if (user == null || user.getId() == null || user.getName() == null) {
            return null;
        }
        // 设置当前时间
        Date nowDate = new Date();
        // 设置到期时间
        Date expireDate = new Date(nowDate.getTime() + 1000 * JwtConstants.EXPIRE);
        return Jwts.builder()
                // 设置Token首部参数信息
                .setHeaderParam("typ", "JWT")
                // 设置Token主题
                .setSubject(JwtConstants.SUBJECT)
                // 设置Token中间字段, 携带用户信息
                .claim("id", user.getId())
                .claim("name", user.getName())
                // 设置签发时间
                .setIssuedAt(nowDate)
                // 设置过期时间
                .setExpiration(expireDate)
                // 设置签名防止篡改, SECRET加密
                .signWith(SignatureAlgorithm.HS512, JwtConstants.SECRET)
                // 创建jwt
                .compact();
    }

    /**
     * 解析jwt
     *
     * @param jwt jwt
     * @return claims
     */
    public static Claims getClaimByToken(String jwt) {
        return Jwts.parser()
                .setSigningKey(JwtConstants.SECRET)
                .parseClaimsJws(jwt)
                .getBody();
    }

    /**
     * 检测jwt是否过期
     *
     * @param claims claims
     * @return bool
     */
    public static boolean isTokenExpired(Claims claims) {
        return claims.getExpiration() // 获取Token过期时间
                .before(new Date()); // 判断当前时间是否在过期时间之前
    }

}