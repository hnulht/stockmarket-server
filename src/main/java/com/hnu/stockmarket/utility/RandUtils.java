package com.hnu.stockmarket.utility;

import lombok.extern.slf4j.Slf4j;

/**
 * 随机数工具
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
public class RandUtils {

    private static final String SALT_CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 随机生成8个字母的盐值
     *
     * @return 8个字母的盐值
     */
    public static String getSalt() {
        StringBuilder builder = new StringBuilder();
        int len = SALT_CHARSET.length();
        for (int i = 0; i < 8; i++) {
            int index = (int) (Math.random() * (len) - 1);
            builder.append(SALT_CHARSET.charAt(index));
        }
        return builder.toString();
    }
    
}