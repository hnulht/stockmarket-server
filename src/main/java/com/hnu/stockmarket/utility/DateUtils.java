package com.hnu.stockmarket.utility;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * 日期工具类
 *
 * @author hnulht
 * @since 2021-07-13
 */
@Slf4j
public class DateUtils {

    /**
     * 获取昨天日期
     *
     * @return date
     */
    public static String getYesterdayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    /**
     * 获取今天日期
     *
     * @return date
     */
    public static String getTodayDate() {
        Calendar calendar = Calendar.getInstance();
        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

}