package com.hnu.stockmarket.utility;

import io.github.swagger2markup.GroupBy;
import io.github.swagger2markup.Language;
import io.github.swagger2markup.Swagger2MarkupConfig;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Swagger接口文档导出工具
 *
 * @author hnulht
 * @since 2021-07-13
 */
@Slf4j
public class SwaggerUtils {

    /**
     * 生成MarkDown格式的API接口文档
     *
     * @throws MalformedURLException MalformedURLException
     */
    public static void generateMarkDownAPIDoc() throws MalformedURLException {
        Path apiDocPath = Paths.get("doc/api/");
        Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
                .withMarkupLanguage(MarkupLanguage.MARKDOWN)
                .withOutputLanguage(Language.ZH)
                .withPathsGroupedBy(GroupBy.TAGS)
                .build();
        Swagger2MarkupConverter.from(new URL("http://localhost:8081/v2/api-docs"))
                .withConfig(config)
                .build()
                .toFolder(Paths.get(String.valueOf(apiDocPath)));
    }

    /**
     * 执行main方法在指定目录下生成MarkDown格式的API接口文档
     *
     * @param args args
     */
    public static void main(String[] args) throws MalformedURLException {
        generateMarkDownAPIDoc();
    }

}