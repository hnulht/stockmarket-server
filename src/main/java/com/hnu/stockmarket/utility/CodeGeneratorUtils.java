package com.hnu.stockmarket.utility;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Mybatis-Plus代码生成器工具类
 *
 * @author hnulht
 * @since 2021-07-04
 */
@Slf4j
public class CodeGeneratorUtils {

    /**
     * 读取控制台内容
     *
     * @param tip tip
     * @return ipt
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    /**
     * 执行main方法控制台输入模块表名回车自动生成对应项目目录中
     *
     * @param args args
     */
    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir"); // 获取当前工程目录
        gc.setOutputDir(projectPath + "/src/main/java"); // 配置生成文件的输出目录
        gc.setAuthor("hnulht"); // 配置开发人员
        gc.setOpen(false); // 配置是否打开输出目录
        gc.setServiceName("%sService"); // 配置service命名方式, 其中%s为占位符
        gc.setSwagger2(true); // 实体属性Swagger2注解
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/stockmarket?serverTimezone=UTC"); // 配置驱动连接的URL
        dsc.setDriverName("com.mysql.cj.jdbc.Driver"); // 配置驱动名称
        dsc.setUsername("root"); // 配置数据库连接用户名
        dsc.setPassword("root"); // 配置数据库连接密码
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.hnu.stockmarket"); // 配置父包名, 如果为空, 将下面子包名必须写全部, 否则就只需写子包名
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        // 模板引擎采用freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 自定义输出文件配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名, 如果Entity设置了前后缀、此处注意xml的名称会跟着发生变化
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName() + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 模板配置
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel); // 配置数据库表映射到实体的命名策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel); // 配置数据库表字段映射到实体的命名策略, 未指定按照naming执行
        // 公共父类实体类, 默认所有的实体类都继承BaseEntity
        strategy.setSuperEntityClass("BaseEntity"); // 配置自定义继承的Entity类全称, 带包名
        strategy.setEntityLombokModel(true); // 配置实体是否为lombok模型(默认false)
        strategy.setRestControllerStyle(true); // 配置生成@RestController控制器
        // 公共父类控制器类, 默认所有的控制器类都继承BaseController
        strategy.setSuperControllerClass("BaseController"); // 配置自定义继承的Controller类全称, 带包名
        // 写于父类中的公共字段
        strategy.setSuperEntityColumns("id", "create_time", "update_time"); // 配置自定义基础的Entity类, 公共字段
        strategy.setInclude(scanner("表名, 多个英文逗号分割").split(",")); // 配置需要包含的表名, 当enableSqlFilter为false时, 允许正则表达式(与exclude二选一配置)
        strategy.setControllerMappingHyphenStyle(true); // 配置驼峰转连字符
        mpg.setStrategy(strategy);

        mpg.setTemplateEngine(new FreemarkerTemplateEngine()); // 配置模板引擎
        mpg.execute();
    }

}