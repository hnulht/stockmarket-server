package com.hnu.stockmarket.utility;

import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * 哈希工具类
 *
 * @author hnulht
 * @since 2021-07-05
 */
@Slf4j
public class HashUtils {

    /**
     * 将传入的字节序列使用 SHA256 算法进行摘要
     *
     * @param bytes 进行摘要的字节序列
     * @return 摘要结果
     */
    public static byte[] sha256(byte[] bytes) {
        if (bytes == null)
            return null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            return messageDigest.digest(bytes);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * 将传入的字节序列使用 BASE64 编码方式进行编码
     *
     * @param bytes 进行编码的字节序列
     * @return 编码生成的字符串
     */
    public static String encodeBase64(byte[] bytes) {
        if (bytes == null)
            return null;
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(bytes);
    }

    /**
     * 加密密码
     *
     * @param password 密码
     * @param salt     盐值
     * @return 加密后的密码
     */
    public static String hashPassword(String password, String salt) {
        String tmp = password + salt;
        return encodeBase64(sha256(tmp.getBytes()));
    }

}