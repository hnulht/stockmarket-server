package com.hnu.stockmarket.mapper;

import com.hnu.stockmarket.entity.StockIndex;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 股票指数数据Mapper接口
 *
 * @author hnulht
 * @since 2021-07-14
 */
public interface StockIndexMapper extends BaseMapper<StockIndex> {

}