package com.hnu.stockmarket.mapper;

import com.hnu.stockmarket.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户Mapper接口
 *
 * @author hnulht
 * @since 2021-07-05
 */
public interface UserMapper extends BaseMapper<User> {

}