package com.hnu.stockmarket.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.stockmarket.entity.StockBasic;

/**
 * 证券基本资料Mapper接口
 *
 * @author hnulht
 * @since 2021-07-11
 */
public interface StockBasicMapper extends BaseMapper<StockBasic> {

}