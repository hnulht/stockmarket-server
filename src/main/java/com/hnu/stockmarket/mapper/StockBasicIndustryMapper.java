package com.hnu.stockmarket.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hnu.stockmarket.entity.StockBasicIndustry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 证券基本资料行业分类Mapper接口
 *
 * @author hnulht
 * @since 2021-07-12
 */
@Mapper
@Component
public interface StockBasicIndustryMapper extends BaseMapper<StockBasicIndustry> {

    // 多表连接查询并筛选上市股票
    String listSQL = "SELECT sb.code, updateDate, sb.code_name, industry, industryClassification, ipoDate, outDate, type, status\n" +
            " FROM stock_industry si\n" +
            " LEFT JOIN stock_basic sb\n" +
            " ON si.code = sb.code AND si.code_name = sb.code_name\n" +
            " WHERE sb.status = 1";

    /**
     * 分页查询
     *
     * @param stockBasicIndustryIPage IPage
     * @return IPage
     */
    @Select(listSQL)
    IPage<StockBasicIndustry> listByPage(IPage<StockBasicIndustry> stockBasicIndustryIPage);

    // 多表连接查询并筛选上市股票且符合模糊查询结果
    String querySQL = "SELECT sb.code, updateDate, sb.code_name, industry, industryClassification, ipoDate, outDate, type, status\n" +
            " FROM stock_industry si\n" +
            " LEFT JOIN stock_basic sb\n" +
            " ON si.code = sb.code AND si.code_name = sb.code_name\n" +
            " WHERE sb.status = 1\n" +
            " AND ( sb.code_name LIKE #{queryInput}\n" +
            " OR sb.CODE LIKE #{queryInput}\n" +
            " OR industry LIKE #{queryInput} )";

    /**
     * 普通查询
     *
     * @return list
     */
    @Select(querySQL)
    List<StockBasicIndustry> listQuery(String queryInput);

}