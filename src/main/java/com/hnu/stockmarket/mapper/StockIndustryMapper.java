package com.hnu.stockmarket.mapper;

import com.hnu.stockmarket.entity.StockIndustry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 行业分类Mapper接口
 *
 * @author hnulht
 * @since 2021-07-11
 */
public interface StockIndustryMapper extends BaseMapper<StockIndustry> {

}