package com.hnu.stockmarket.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.stockmarket.entity.StockChg;

/**
 * 涨跌幅Mapper接口
 *
 * @author hnulht
 * @since 2021-07-13
 */
public interface StockChgMapper extends BaseMapper<StockChg> {

    // 涨跌幅插入
    String saveSQL = "INSERT INTO stock_chg ( CODE, code_name, industry )\n" +
            " SELECT sb.CODE, sb.code_name, industry\n" +
            " FROM stock_industry si\n" +
            " LEFT JOIN stock_basic sb\n" +
            " ON si.code = sb.code AND si.code_name = sb.code_name\n" +
            " WHERE sb.status = 1";

    // 涨幅查询
    String riseSQL = "SELECT * FROM stock_chg WHERE pctChg not like \"-%\" ORDER BY pctChg DESC;";

    // 跌幅查询
    String fallSQL = "SELECT * FROM stock_chg WHERE pctChg like \"-%\" ORDER BY pctChg DESC;";

}