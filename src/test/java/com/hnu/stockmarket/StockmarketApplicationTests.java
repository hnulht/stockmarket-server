package com.hnu.stockmarket;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hnu.stockmarket.entity.StockBasicIndustry;
import com.hnu.stockmarket.entity.StockChg;
import com.hnu.stockmarket.entity.StockIndustry;
import com.hnu.stockmarket.entity.User;
import com.hnu.stockmarket.service.*;
import com.hnu.stockmarket.utility.JwtUtils;
import org.junit.jupiter.api.Test;
import org.python.util.PythonInterpreter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class StockmarketApplicationTests {

    @Autowired
    UserService userService;

    @Autowired
    StockIndustryService stockIndustryService;

    @Autowired
    StockBasicService stockBasicService;

    @Autowired
    StockChgService stockChgService;

    @Autowired
    private StockBasicIndustryService stockBasicIndustryService;

    @Test
    void test01() {
        System.out.println("TOKEN: " + JwtUtils.generateToken(userService.getById(1)));
        System.out.println("USER: " + userService.getById(1));
    }

    @Test
    void test02() {
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.exec("print(\"HelloWorld\")");
    }

    @Test
    void test03() {
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.execfile("src/main/resources/pythonscript/HelloWorld.py");
    }


    @Test
    void test04() {
        User user = new User();
        user.setId((long) 3);
        user.setName("odwdjj");
        user.setPassword("dhdhwd");
        user.setPhoneNumber("114514");
        user.setRole(0);
        user.setLastLoginTime(LocalDateTime.now());
        userService.updateById(user);
    }

    @Test
    void test05() {
        IPage<StockIndustry> stockIndustryIPage = new Page<>(2, 10);
        System.out.println(stockIndustryService.page(stockIndustryIPage));
        System.out.println(stockIndustryService.list());
    }

    @Test
    void test06() {
        IPage<StockBasicIndustry> page = new Page<>(2, 10);
        System.out.println(stockBasicIndustryService.listByPage(page).getRecords());
        System.out.println(stockBasicIndustryService.listByPage(page).getTotal());
    }

    @Test
    void test07() {
        ArrayList<StockChg> stockChgList = (ArrayList<StockChg>) stockChgService.list();
        for (StockChg stockChg : stockChgList) {
            System.out.println(stockChg.toString());
        }
        System.out.println(stockChgList.size());
    }

    @Test
    void test08() {
        StockChg newStockChg = new StockChg();
        newStockChg.setCode("sh.600000");
        newStockChg.setCodeName("浦发银行");
        newStockChg.setIndustry("银行");
        newStockChg.setPctChg("0.000000");
        Wrapper<StockChg> stockChgWrapper = new QueryWrapper<StockChg>().eq("code", "sh.600000");
        System.out.println(stockChgService.list(stockChgWrapper));
        stockChgService.update(newStockChg, stockChgWrapper);
    }

    @Test
    void test09() {
        String queryInput = "银行";
        Wrapper<StockChg> stockChgWrapper = new QueryWrapper<StockChg>().like("code_name", queryInput).or().like("code", queryInput).or().like("industry", queryInput);
        List<StockChg> stockChgList = stockChgService.list(stockChgWrapper);
        System.out.println(stockChgList);
    }

    @Test
    void test10() {
        String queryInput = "电子";
        List<StockBasicIndustry> stockBasicIndustryList = stockBasicIndustryService.listQuery(queryInput);
        System.out.println(stockBasicIndustryList);
    }

}